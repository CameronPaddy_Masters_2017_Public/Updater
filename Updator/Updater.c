// Updator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestList.h"
#include "csprng.h"

TCHAR *randstring(size_t length, BOOL truRand) {
	static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	TCHAR *randomString = NULL;
	CSPRNG rng2 = NULL;
	rng2 = csprng_create(rng2);
	if (!rng2)
		return 1;

	if (length) {
		randomString = malloc(sizeof(TCHAR) * (length + 1) + 10);
		if (randomString) {
			for (int n = 0; n < length; n++) {
				int key;
				if (truRand == TRUE) {
					key = csprng_get_int(rng2);
					key %= (int)(sizeof(charset) - 1);
				}
				else
					key = rand() % (int)(sizeof(charset) - 1);

				randomString[n] = charset[key];
			}
			randomString[length] = '\0';
		}
	}
	return randomString;
}


void ListDirectoryFiles(TCHAR *directory, List* list) {
	HANDLE handle;
	WIN32_FIND_DATA file;

	TCHAR searchString[100];
	wcscpy(searchString, directory);
	wcscat(searchString, L"*");
	TCHAR filepath[MAX_PATH];

	handle = FindFirstFile(searchString, &file);
	searchString[wcslen(searchString) - 1] = 0;

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			wcscpy(filepath, directory);
			wcscat(filepath, file.cFileName);
			prepend_list(list, &filepath);
		}
	}
}


int main(int argc, char *argv[])
{
	int status;
	long msPause = 0;
	List testList;
	CSPRNG rng = NULL;
	init_list(&testList);
	unsigned long randomDelay = 0;
	BOOL truRand = FALSE;

	if (argc > 1) {
		errno = 0;
		msPause = strtol(argv[1], NULL, 10);
		if (errno != 0) {
			printf("Error! Please enter only number values in ms.");
			return -1;
		}
	}

	if (msPause > 20000)
		msPause = 0;

	rng = csprng_create(rng);
	if (!rng)
		return 1;

	TCHAR base[100] = L"C:\\ProggerTests";
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\Updater");
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\");

	ListDirectoryFiles(base, &testList);
	TCHAR *random;

	for (int i = 0; i < 20000; i++) {

		if (msPause == -1) {
			randomDelay = csprng_get_int(rng);
			randomDelay %= 5001;
			Sleep(randomDelay);
		}
		else
			Sleep(msPause);

		int myInt = rand() % list_size(&testList);
		ListNode *tmp = select_node(&testList, myInt);
		TCHAR toUse[MAX_PATH];
		wcscpy(toUse, tmp->filename);
		char usablePath[MAX_PATH];
		wcstombs(usablePath, toUse, MAX_PATH);
		random = randstring(100, FALSE);
		FILE *fp = fopen(usablePath, "a+");
		fprintf(fp, "%ws\r\n", random);
		fclose(fp);
		free(random);
	}

    return 0;
}

